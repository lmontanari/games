# Games

## Swagger:
http://localhost:8080/swagger-ui/index.html#/

## Getting started

### 1. Create a mysql database using docker:

docker run --name games-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d -p 3306:3306 mysql

#### 1.2 Create user to access database by executing the following commands:

docker exec -it games-mysql bash
mysql -p
my-secret-pw
create user 'lmontanari'@'172.17.0.1' identified by 'test111';
grant all privileges on thisdb._ to 'lmontanari'@'172.17.0.1';
FLUSH PRIVILEGES;
GRANT SELECT,INSERT,UPDATE ON _ . \* TO 'lmontanari'@'172.17.0.1';

#### 1.2.1 Create database:

CREATE DATABASE `games` /_!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci _/ /_!80016 DEFAULT ENCRYPTION='N' _/;

#### 1.2.2 Create tables:

CREATE TABLE games.GAME (
`ID` varchar(100) NOT NULL,
`TITLE` varchar(100) NOT NULL,
`YEAR` int unsigned NOT NULL,
`COMPLETED` tinyint(1) NOT NULL DEFAULT '0',
`DATE_OF_COMPLETION` date DEFAULT NULL,
`NOTES` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE games.CONSOLE (
ID INT UNSIGNED NOT NULL,
NAME varchar(100) NOT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;
ALTER TABLE games.CONSOLE ADD CONSTRAINT CONSOLE_PK PRIMARY KEY (ID);

ALTER TABLE games.GAME ADD ID_CONSOLE INT UNSIGNED NOT NULL;
ALTER TABLE games.GAME ADD CONSTRAINT GAME_PK PRIMARY KEY (ID);
ALTER TABLE games.GAME ADD CONSTRAINT GAME_FK FOREIGN KEY (ID_CONSOLE) REFERENCES games.CONSOLE(ID);

#### 1.2.3 Insert data:

use games;
insert into games.CONSOLE values (1, 'PS1');
insert into games.CONSOLE values (2, 'PS2');
insert into games.CONSOLE values (3, 'PS3');
insert into games.CONSOLE values (4, 'PS4');
insert into games.CONSOLE values (5, 'PS5');

insert into games.GAME values ('f404c038-7af5-11ec-90d6-0242ac120003', 'Metal Gear Solid 2', 2001, true, DATE('2017-08-07'), 'I really liked this game. A masterpiece from Kojima productions', 4);
insert into games.GAME values ('fe1b6ce4-82c9-11ec-a8a3-0242ac120002', 'Final Fantasy XIII', 2009, true, DATE('2010-08-01'), '100 hours to complete this game', 1);

### 2. Compile backend:

Go to games-backend folder and run local gradle to compile:
~/git/games/games-backend$ ./gradlew clean build

#### 2.1 Run backend:

./gradlew bootRun

### 3. Install frontend:

Go to games-frontend folder and run npm install:
~/git/games/games-frontend$ npm install

#### 3.1 Build frontend:

~/git/games/games-frontend$ ng build

#### 3.2 Run frontend:

~/git/games/games-frontend$ npm start

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesListComponent } from './games/games-list/games-list.component';

const routes: Routes = [
    {
        path: 'games',
        component: GamesListComponent
    },
    // {
    //     path: '/add',
    //     component: GamesFormComponent
    // },
    {
        path: '**',
        component: GamesListComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { GamesListModule } from './games-list/games-list.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    GamesListModule,
    // BrowserAnimationsModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  exports: [MatDatepickerModule],
})
export class GamesModule {}

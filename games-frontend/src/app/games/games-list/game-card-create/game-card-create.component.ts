import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Game } from 'src/app/models/game';
import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-game-card-create',
  templateUrl: './game-card-create.component.html',
  styleUrls: ['./game-card-create.component.scss'],
})
export class GameCardCreateComponent implements OnInit {
  @Input() game: Game = {} as Game;
  date: FormControl = new FormControl(new Date());
  cardForm: FormGroup = this.formBuilder.group({
    title: '',
    year: '',
    console: '',
    notes: '',
    completed: false,
    date: new Date(),
  });

  constructor(
    private formBuilder: FormBuilder,
    private gameService: GamesService,
    private dlgRef: MatDialogRef<any>
  ) {}

  ngOnInit(): void {
    this.date.setValue(new Date(this.game.dateOfCompletion));
  }

  getFormControl(name: string): FormControl {
    return this.cardForm?.get(name) as FormControl;
  }

  save() {
    const game = {
      title: this.getFormControl('title').value,
      year: this.getFormControl('year').value,
      completed: this.getFormControl('completed').value,
      dateOfCompletion: this.getFormControl('date').value,
      notes: this.getFormControl('notes').value,
      consoleId: this.getFormControl('console').value,
    };
    this.gameService.addGame(game).subscribe(() => {
      this.dlgRef.close();
    });
  }
}

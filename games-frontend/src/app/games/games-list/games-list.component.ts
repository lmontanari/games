import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { Game } from 'src/app/models/game';
import { debounceTime, filter } from 'rxjs/operators';

import { GamesService } from '../../services/games.service';
import { MatDialog } from '@angular/material/dialog';
import { GameCardCreateComponent } from './game-card-create/game-card-create.component';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnInit, OnDestroy {
  // TODO create header component
  // search box:
  // onTyping = new EventEmitter<string>(); // @Output()
  // value: string = ''; //@Input()
  debounce: Subject<KeyboardEvent> = new Subject<KeyboardEvent>();
  value: string = '';

  gamesList: Game[] = [];

  constructor(private gamesService: GamesService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.loadGames();

    this.debounce
      .pipe(debounceTime(300))
      .subscribe((gameToFilter) => this.loadGamesWithFilter(gameToFilter));
    // .subscribe((filter) => this.onTyping.emit(filter));
  }
  loadGamesWithFilter(gameToFilter: KeyboardEvent) {
    this.gamesService.listAllGames().subscribe((games) => {
      this.gamesList = games.filter((game) => {
        if (gameToFilter.target) {
          return game.title
            .toLowerCase()
            .includes((gameToFilter.target as any).value);
        }
        return true;
      });
    });
  }
  openDialog() {
    const dialogRef = this.dialog.open(GameCardCreateComponent);

    dialogRef.afterClosed().subscribe((result) => {
      this.loadGames();
    });
  }
  loadGames() {
    this.gamesService.listAllGames().subscribe((games) => {
      console.log(games);
      this.gamesList = games;
    });
  }
  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }
}

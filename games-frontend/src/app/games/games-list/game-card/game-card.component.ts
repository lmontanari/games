import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Game } from 'src/app/models/game';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss'],
})
export class GameCardComponent implements OnInit {
  @Input() game: Game = {} as Game;
  date: FormControl = new FormControl(new Date());

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.date.setValue(new Date(this.game.dateOfCompletion));
  }

  calculateAge(): number {
    return new Date().getFullYear() - this.game.year;
  }
}

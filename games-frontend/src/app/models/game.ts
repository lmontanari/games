// completed: false
// console: "PS1"
// dateOfCompletion: null
// id: "62a6d7eb-f8b1-4ccb-b59f-40a4e742a452"
// notes: "test"
// title: "test"
// year: 2010
export interface Game {
    id: string;
    dateOfCompletion:Date;
    notes:string;
    console:string;
    title:string;
    year:number;
    completed:boolean;
}

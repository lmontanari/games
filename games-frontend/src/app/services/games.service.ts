import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Game } from '../models/game';

const API = 'http://localhost:8080';

@Injectable({ providedIn: 'root' })
export class GamesService {
  constructor(private http: HttpClient) {}

  listAllGames(): Observable<Game[]> {
    return this.http.get<Game[]>(API + '/game');
  }

  addGame(game: GameRequest): Observable<void> {
    console.log(game);
    const url = API + '/game';
    let params = new HttpParams();
    params = params.set('title', game.title);
    params = params.set('year', game.year);
    params = params.set('notes', game.notes);
    params = params.set('consoleId', game.consoleId);
    params = params.set('completed', game.completed);
    params = params.set(
      'dateOfCompletion',
      game.dateOfCompletion.toISOString().split('T')[0]
    );
    return this.http.post<void>(url, params);
  }
}
export interface GameRequest {
  dateOfCompletion: Date;
  notes: string;
  consoleId: string;
  title: string;
  year: number;
  completed: boolean;
}

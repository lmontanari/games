package com.lmontanari.games.repository;

import com.lmontanari.games.model.Game;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, String> {

	@EntityGraph(
			type = EntityGraph.EntityGraphType.LOAD,
			attributePaths = {
					"console"
			}
	)
	List<Game> findAll();
}

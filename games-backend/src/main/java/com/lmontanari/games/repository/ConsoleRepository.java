package com.lmontanari.games.repository;

import com.lmontanari.games.model.Console;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsoleRepository extends CrudRepository<Console, Integer> {
	List<Console> findAll();
}

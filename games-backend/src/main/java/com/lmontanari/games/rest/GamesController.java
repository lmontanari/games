package com.lmontanari.games.rest;

import com.lmontanari.games.model.Game;
import com.lmontanari.games.rest.dto.GameRequest;
import com.lmontanari.games.rest.dto.GameResponse;
import com.lmontanari.games.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/game")
public class GamesController {

	private GameService gameService;

	@Autowired
	public GamesController(GameService gameService) {
		this.gameService = gameService;
	}

	@GetMapping()
	public ResponseEntity<List<GameResponse>> findAllGames() {
		List<Game> games = this.gameService.findAllGames();
		return ResponseEntity.ok(games.stream().map(game -> this.convert(game))
				.collect(Collectors.toList()));
	}

	@PostMapping
	public ResponseEntity createNewGame(GameRequest request) {
		final var game = this.convert(request);
		this.gameService.createNewGame(game, request.getConsoleId());

		return ResponseEntity.noContent().build();
	}

	private GameResponse convert(Game game) {
		return GameResponse.builder().id(game.getId()).console(game.getConsole().getName()).title(game.getTitle()).year(game.getYear()).completed(game.isCompleted()).dateOfCompletion(game.getDateOfCompletion()).notes(game.getNotes()).build();
	}

	private Game convert(GameRequest request) {
		return Game.builder().title(request.getTitle()).year(request.getYear()).completed(request.isCompleted()).dateOfCompletion(LocalDate.parse(request.getDateOfCompletion())).notes(request.getNotes()).build();
	}
}

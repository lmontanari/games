package com.lmontanari.games.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameRequest {

	@NotBlank(message = "Title is mandatory")
	@Size(max = 100)
	private String title;

	@NotBlank(message = "Year is mandatory")
	@Min(1970)
	private Integer year;
	
	private boolean completed;

	private String dateOfCompletion;
	private String notes;

	private Integer consoleId;
}

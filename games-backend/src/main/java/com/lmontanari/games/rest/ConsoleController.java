package com.lmontanari.games.rest;

import com.lmontanari.games.model.Console;
import com.lmontanari.games.rest.dto.ConsoleResponse;
import com.lmontanari.games.service.ConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/console")
public class ConsoleController {

	private ConsoleService consoleService;

	@Autowired
	public ConsoleController(ConsoleService consoleService) {
		this.consoleService = consoleService;
	}

	@GetMapping()
	public List<ConsoleResponse> findAllConsoles() {
		List<Console> consoles = this.consoleService.findAllConsoles();
		return consoles.stream().map(console -> this.convert(console))
				.collect(Collectors.toList());
	}

	private ConsoleResponse convert(Console console) {
		return ConsoleResponse.builder().id(console.getId()).name(console.getName()).build();
	}
}

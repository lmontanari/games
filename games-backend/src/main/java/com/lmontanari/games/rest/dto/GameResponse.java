package com.lmontanari.games.rest.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class GameResponse {

	private String id;
	private String title;
	private Integer year;
	private boolean completed;
	private LocalDate dateOfCompletion;
	private String notes;
	private String console;
}


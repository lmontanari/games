package com.lmontanari.games.service;

import com.lmontanari.games.model.Console;
import com.lmontanari.games.model.Game;
import com.lmontanari.games.repository.ConsoleRepository;
import com.lmontanari.games.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class GameService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private ConsoleService consoleService;

	public List<Game> findAllGames() {
		return this.gameRepository.findAll();
	}

	public void createNewGame(Game game, Integer consoleId) {
		Console console =
				consoleService.findById(consoleId).orElseThrow(() -> new RuntimeException("Console not found"));
		game.setConsole(console);
		game.setId(UUID.randomUUID().toString());
		if(!game.isCompleted()) {
			game.setDateOfCompletion(null);
		}
		this.gameRepository.save(game);
	}


}

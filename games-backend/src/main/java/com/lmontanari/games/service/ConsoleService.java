package com.lmontanari.games.service;

import com.lmontanari.games.model.Console;
import com.lmontanari.games.repository.ConsoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConsoleService {

	@Autowired
	private ConsoleRepository consoleRepository;

	public List<Console> findAllConsoles() {
		return this.consoleRepository.findAll();
	}

	public Optional<Console> findById(Integer id) {
		return this.consoleRepository.findById(id);
	}

}

package com.lmontanari.games.model;

import com.lmontanari.games.rest.dto.ConsoleResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "GAME")
public class Game {

	@Id
	@EqualsAndHashCode.Include
	private String id;

	@Column
	private String title;

	@Column
	private Integer year;

	@Column
	private boolean completed;

	@Column(name = "DATE_OF_COMPLETION")
	private LocalDate dateOfCompletion;

	@Column
	private String notes;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CONSOLE")
	private Console console;
}

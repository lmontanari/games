package com.lmontanari.games.service;

import com.lmontanari.games.model.Console;
import com.lmontanari.games.repository.ConsoleRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@DisplayName("console service tests")
@ExtendWith(MockitoExtension.class)
class ConsoleServiceTest {

		@InjectMocks
		private ConsoleService consoleService;

		@Mock
		private ConsoleRepository consoleRepository;

		@Test
		void testFindAllConsoles() {
			final var ps3 = Console.builder().id(3).name("PS3").build();
			final var consoleList = List.of(ps3);
			when(consoleRepository.findAll()).thenReturn(consoleList);
			final var consoles = consoleService.findAllConsoles();
			assertEquals(3, consoles.get(0).getId());
		}
	}

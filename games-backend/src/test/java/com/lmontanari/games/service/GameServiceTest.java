package com.lmontanari.games.service;

import com.lmontanari.games.model.Game;
import com.lmontanari.games.repository.ConsoleRepository;
import com.lmontanari.games.repository.GameRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@DisplayName("game service tests")
@ExtendWith(MockitoExtension.class)
class GameServiceTest {

	@InjectMocks
	private GameService gameService;

	@Mock
	private ConsoleRepository consoleRepository;

	@Mock
	private GameRepository gameRepository;

	@Test
	void testFindAllGames() {
		final var game = Game.builder().id("testuuid").title("game title").notes("notes").year(2020).build();
		final var gameList = List.of(game);
		when(gameRepository.findAll()).thenReturn(gameList);
		final var games = gameService.findAllGames();
		assertEquals("testuuid", games.get(0).getId());
	}


}

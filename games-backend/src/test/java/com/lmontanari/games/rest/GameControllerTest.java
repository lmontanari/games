package com.lmontanari.games.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lmontanari.games.model.Console;
import com.lmontanari.games.model.Game;
import com.lmontanari.games.rest.dto.GameRequest;
import com.lmontanari.games.service.ConsoleService;
import com.lmontanari.games.service.GameService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.RequestEntity.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("game controller tests")
@WebMvcTest(controllers = GamesController.class)
class GameControllerTest {

	@MockBean
	private ConsoleService consoleService;

	@MockBean
	private GameService gameService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testFindAllGames() throws Exception {
		when(gameService.findAllGames()).thenReturn(List.of(Game.builder().id("test").console(Console.builder().id(3).build()).title("title").completed(false).notes("notes").year(2020).build()));

		mockMvc.perform(get("/game")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is("test")));

	}


}

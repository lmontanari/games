package com.lmontanari.games.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lmontanari.games.model.Console;
import com.lmontanari.games.rest.dto.ConsoleResponse;
import com.lmontanari.games.service.ConsoleService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("console controller tests")
@WebMvcTest(controllers = ConsoleController.class)
class ConsoleControllerTest {

	@MockBean
	private ConsoleService consoleService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testFindAllConsoles() throws Exception {
		when(consoleService.findAllConsoles()).thenReturn(List.of(Console.builder().id(3).name("test").build()));

		final MvcResult mvcResult = mockMvc.perform(get("/console")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		verify(consoleService, times(1)).findAllConsoles();

		final List response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), List.class);
		assertEquals(1, response.size());
	}
}
